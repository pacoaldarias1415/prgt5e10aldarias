/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ejemplo0530;

/**
 * Fichero: Figura.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-nov-2013
 */

class Figura {
   private String color;
   
   public void setColor(String s) {
      color = s;
   }
   public String getColor() {
      return color;
   }

}

