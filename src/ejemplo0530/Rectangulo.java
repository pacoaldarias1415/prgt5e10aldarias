/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ejemplo0530;

/**
 * Fichero: Rectangulo.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-nov-2013
 */

public class Rectangulo extends Figura {
  
   private int base;
   private int altura;
   
   Rectangulo (int ba, int al) {
      base=ba;
      altura=al;
   }
   public int area() {
      return base*altura;
   }
   public void mostrar() { // Función que no esta en Figura
      System.out.println("Rectangulo: Color: "+ getColor() +" Base: "+base+
                         " Altura: "+altura);
   }

}