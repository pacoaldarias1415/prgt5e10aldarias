/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ejemplo0510;

/**
 * Fichero: Cohete.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 24-nov-2013
 */

public class Cohete {

/**
 * Variables Miembros Static de un clase
 * Las variables miembro static mantienen
 * su valor.
 */

  private static int numcohetes = 0;
  Cohete() {
    numcohetes++;
  }
  public int getcohetes() {
    return numcohetes;
  }
  public void lanza() {
    numcohetes++;
  }

}