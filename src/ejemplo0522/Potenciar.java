/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0522;

/**
 * Fichero: Potenciar.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
public class Potenciar {

  public static int potencia(int x, int y) {
    if (y == 1) {
      return x;
    } else {
      return x * potencia(x, y - 1);
    }
  }

  public static void main(String[] args) {
    System.out.println(potencia(2, 3));
  }
}

/*EJECUCION:
 8
 */
