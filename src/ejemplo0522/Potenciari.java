/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0522;

/**
 * Fichero: Potenciai.java Calcula la potencia en iterativo.
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
public class Potenciari {

  public static int potenciai(int x, int y) {

    int res = 1, i;
    for (i = 1; i <= y; i++) {
      res = res * x;
    }
    return res;

  }

  public static void main(String[] args) {
    System.out.println(potenciai(2, 3));
  }
}

/*EJECUCION:
 8
 */
