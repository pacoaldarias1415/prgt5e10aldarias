/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0501;

import ejemplo0501.utilidades.educacion.Saludar;
import ejemplo0501.utilidades.educacion.Despedirse;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 25-nov-2013
 */
public class Main {

    public static void main(String[] args) {

        Saludar s = new Saludar();
        Despedirse d = new Despedirse();
        s.saludo();
        d.despedida();
    }
}
