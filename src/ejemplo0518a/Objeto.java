
package ejemplo0518a;

/**
 * Fichero: Objeto.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 03-dic-2013
 */

public class Objeto {
   public int a;
   
   public void funcion( Objeto b ){
      b.a=1;
   }
   
}
