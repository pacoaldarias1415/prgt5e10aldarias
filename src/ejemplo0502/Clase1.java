/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0502;

/**
 * Fichero: Clase1.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 23-nov-2013
 */
class Clase1 { // Accesible desde clases de su paquete 
// public class Clase1 {  // Accesible desde clases de otro paquetes

  // public int x=1; // No encapsula
  private int x = 1;

  public int getX() {
    return x;
  }
}
